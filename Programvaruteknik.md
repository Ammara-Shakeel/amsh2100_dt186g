# Programvaruteknik, 120-180 hp #
**Hösten 2016 tar vi in nya studenter till vårt populära program Programvaruteknik vid Mittuniversitetet i östersund. Nytt är att du kan läsa programmet på Campus i östersund eller på distans! Fokus i programmet är att kunna bygga olika typer av applikationer för Webben, din dator eller för din smartphone. Mobilitet, distansoberoende, webbtjänster och cloud computing ökar i betydelse och där är vi med! Genom att läsa programmet skaffar du dig en gedigen universitetsutbildning som ger dig verktygen att vara med att forma den digitala framtiden.**

Utbildningen ger dig en språngbräda till ett jobb inom IT-branschen. Med modern teknik och metodik lär du dig design och implementering av mjukvara för system som kan bestå av servrar, persondatorer/laptops samt mobiltelefoner och surfplattor. 
## Sammankomster ##
Programmet har tagits fram i samarbete med IT-företag i regionen. För att läsa programmet på distans behöver du en bra internetuppkoppling, ett headset och en webbkamera för att kunna kommunicera. Mötena är därför virtuella och du behöver inte flytta till oss i Östersund. Studerar du på campus har du självklart tillgång till allt distansmaterial utöver alla aktiviteter på plats i vår egen labbhall. Nytt för i år är helt nya lokaler på första våningen med eget klassrum/labrum. Vi delar pentry, soffa och studiehörna med Informatikgänget. De är helt underlägsna oss i programmering, men det säger sig självt.

## Om Programmet ##
Första året innehåller nödvändiga grundkurser som ska leda till en gedigen kompetens inom området. Den objektorienterade programmeringen. Det andra året ägnas åt olika aspekter av utveckling för webben med kopplingar till databaser, bryggning från C++ till Java och applikationer inom området mobila enheter. Under utbildningen kommer du även att arbeta i grupper med ett gemensamt ansvar för olika delar av kedjan från idé till färdig produkt.

Under utbildningen kommer du till största delen att läsa kurser inom ämnesområdet datateknik. Ditt virtuella klassrum i de olika kurserna kommer att vara den webbaserade utbildningsplattformen Moodle. Där hittar du allt kursmaterial och där kommunicerar du med lärare och dina medstudenter på kurserna. Till största delen görs även tester och rapporter via webben.

Utöver teori innehåller våra kurser en stor mängd praktiska uppgifter som ska utföras och redovisas. Det är ofta programmeringsuppgifter som du ska lösa själv eller mindre projekt som du kan samarbeta om med andra elever. Många av kurserna bygger vidare på tidigare kurser och på så sätt fördjupas kunskaperna samtidigt som man tillämpar kunskaper från tidigare kurser. Eftersom allt kursmaterial kommer att finnas tillgängligt i Moodle är du inte bunden till att passa tider för föreläsningar etc. Däremot kommer rapporter och prov att vara bundna till vissa tider.
## Utbildningsplan ##
### _År 1_  ###

    Datavetenskaplig introkurs (LP1)
    Operativsystem introduktion med tillämpningar i Linux (LP1)
    Introduktion till programmering i C++ (LP2)
    Datakommunikation och nätverk med tillämpningar i Linux (LP2)
    Objektbaserad programmering i C++ (LP3)
    Diskret matematik för programmerare (LP3+4)
    Databaser, modellering och implementering (LP3+4)
    Objektorienterad programmering i C++ (LP4)
    Valbar kurs: Avancerad felprogrammering
    
___

### _År 2_ ###

    Metoder och verktyg i mjukvaruprojekt (LP1)
    XML (LP1)
    Java för C++ programmerare (LP2)
    Webbprogrammering med HTML5, CSS3 och JavaScript (LP2)
    Designmönster med C++ (LP3)
    Webbprogrammering med PHP och PostgreSQL (LP3)
    Applikationsutveckling för Android (LP4)
    Säkerhet i mjukvara (LP4)
    Valbar kurs: Vattnets betydelse för sjöfarten
---

### _År 3_ ### 

    Valbar kurs: Murphy's law i teori och praktik
    Presentation av ny teknik (LP1+2)
    Java Enterprise-utveckling med EE-standarden (LP1+2)
    Systemprogrammering i UNIX/Linux (LP1)
    Programmering med samtidighet och parallellism (LP2)
    Artificiell Intelligens för agenter (LP3)
    Tillämpad datateknik, mjukvaruprojekt (LP3)
    Sjävständigt arbete (LP4)

## Examen efter genomförda studier ##

**Kandidatexamen**

Efter tre år kan du ta en kandidatexamen med datateknik som huvudämne och med inriktning på mjukvaruteknik. 

**Högskoleexamen**

Alternativt kan du ta en högskoleexamen med inriktning datateknik efter två år om du väljer att inte läsa tredje året.
